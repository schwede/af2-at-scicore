.. Copyright (c) 2021, SIB - Swiss Institute of Bioinformatics and
..                     Biozentrum - University of Basel
..
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
..
.. The above copyright notice and this permission notice shall be included in all
.. copies or substantial portions of the Software.
..
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

Welcome to af2\@scicore/run_af2's documentation!
=================================================

You are reading the documentation for version |version| (release |release|)
of **run_af2**.

**run_af2** is a Python package to run the `AlphaFold 2 (AF2) pipeline
<https://github.com/deepmind/alphafold>`_ on the sciCORE cluster.

The package comes with two major parts: userland commands to just execute the
AF2 pipeline and Python modules for programmatic access of the AF2 pipeline from
your own scripts. Documentation of the commands can be found on the
`GitLab repository page of af2\@scicore
<https://git.scicore.unibas.ch/schwede/af2-at-scicore>`_. Documentation for the
Python modules is here:

.. toctree::
   :maxdepth: 2

   _shared
   run_singularity


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

..  LocalWords:  af AlphaFold sciCORE userland GitLab
