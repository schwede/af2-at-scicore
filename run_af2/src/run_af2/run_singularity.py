"""Script/ module to run the AlphaFold2 pipeline from a Singularity container.

There are two modes:

* use as Python module

* use as script just executing the Singularity image (available as run-af2)
"""

# Copyright (c) 2021, SIB - Swiss Institute of Bioinformatics and
#                     Biozentrum - University of Basel
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from datetime import datetime, timedelta
import argparse
import os
import re
import subprocess
import sys

from . import _shared


def _parse_cmdline():
    """Parse command line arguments."""
    return _shared.parse_af2_arguments(
        argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description=__doc__,
        )
    )


def _prepare_singularity_bind_mount(path, already_bound):
    """Create a bind mount for path outside of $HOME.

    Singularity mounts $HOME by default, no need to mount paths in there."""
    mnt_prfx = "/mnt"
    abs_path = os.path.abspath(path)
    home = os.getenv("HOME")
    if abs_path.startswith(home):
        return abs_path, already_bound

    src_dir = os.path.dirname(abs_path)
    for av_src in already_bound:
        cpt = os.path.commonpath([av_src, src_dir])
        if cpt == os.path.sep:
            continue
        if cpt not in already_bound:
            mnt_dir = os.path.join(mnt_prfx, cpt[1:])
            del already_bound[av_src]
            already_bound[cpt] = mnt_dir
        trg_path = os.path.join(mnt_prfx, abs_path[1:])
        return trg_path, already_bound

    mnt_dir = os.path.join(mnt_prfx, src_dir[1:])
    already_bound[src_dir] = mnt_dir
    trg_path = os.path.join(mnt_prfx, abs_path[1:])

    return trg_path, already_bound


def _collect_bind_mountpints(tmpdir, fasta_files, output_dir, data_paths):
    """Collect path to mount into the container and make sure that common paths
    only get one mountpoint.

    For input FASTAS files, also create mountpoints and adapt the input paths to
    the mountpoint inside the container.
    """
    binds = {
        tmpdir: tmpdir,
    }

    # prepare input files, may need extra mounting
    snglrty_fasta = []
    for fasta in fasta_files:
        bind_fasta, binds = _prepare_singularity_bind_mount(fasta, binds)
        snglrty_fasta.append(bind_fasta)

    # prepare ouput dir, may need extra mount
    bind_out, binds = _prepare_singularity_bind_mount(output_dir, binds)

    # deal with paths of all the databases
    for flag, path in data_paths.items():
        path, binds = _prepare_singularity_bind_mount(path, binds)
        data_paths[flag] = path

    return binds, snglrty_fasta, bind_out, data_paths


def _assemble_singularity_call(  # pylint: disable=too-many-arguments
    snglrty_fasta,
    snglrty_out,
    max_template_date,
    af2_image,
    snglrty_bin,
    tmpdir,
    binds,
    use_gpu,
    db_preset,
    model_preset,
    use_precomputed_msas,
    use_gpu_relax,
    num_multimer_predictions_per_model,
    data_paths,
    extra_arg_list,
):
    """Assemble the command to run AF2 from the Singularity image."""
    # Since the number of arguments to this function is already high, we allow
    # more local variables than defined by PEP8.
    # pylint: disable=too-many-locals

    # Since a lot of applications use /tmp hard-coded, we mount the actual
    # directory for temporary data as set by the host system.
    snglrty_cmd = [
        snglrty_bin,
        "run",
        "--bind",
        f"{tmpdir}:/tmp",
        "--pwd",
        "/app/alphafold",
    ]

    for src_dir, dest_dir in binds.items():
        snglrty_cmd.extend(["--bind", f"{src_dir}:{dest_dir}"])

    if use_gpu:
        snglrty_cmd.append("--nv")

    snglrty_cmd.extend(
        [
            f"{af2_image}",
            f"--fasta_paths={','.join(snglrty_fasta)}",
            f"--output_dir={snglrty_out}",
            f"--max_template_date={max_template_date}",
            f"--db_preset={db_preset}",
            f"--model_preset={model_preset}",
            "--num_multimer_predictions_per_model="
            + f"{num_multimer_predictions_per_model}",
            "--logtostderr",
        ]
    )

    bool_args = {
        "use_precomputed_msas": use_precomputed_msas,
        "use_gpu_relax": use_gpu_relax,
    }
    for arg, val in bool_args.items():
        snglrty_cmd.append(f"--{arg}={str(val)}")

    for flag, path in data_paths.items():
        snglrty_cmd.append(f"--{flag}={path}")

    # append extra arguments
    snglrty_cmd.extend(extra_arg_list)

    return snglrty_cmd


def _determine_af2_image(af2_image_dir):
    """Select an image from a given directory. File name of the images need to
    be of form alphafold-<YYYY-MM-DD>.sif. The path to the latest image will be
    returned, determined by the '<YYYY-MM-DD>' component of the file name. If
    the file name is of different form, an arbitrary file ending with '.sif'
    will be used."""
    lst_mtch = "0000-00-00"
    af2_path = ""
    af2_files = os.listdir(af2_image_dir)
    for af2_file in af2_files:
        mtch = re.match(
            r"alphafold[-_](\d{4}-\d{2}-\d{2}|latest).sif", af2_file
        )
        if mtch:
            if mtch.group(1) == "latest":
                af2_path = os.path.join(af2_image_dir, af2_file)
                return os.path.realpath(af2_path)
            if mtch.group(1) > lst_mtch:
                af2_path = af2_file
                lst_mtch = mtch.group(1)
        elif af2_file.endswith(".sif"):
            af2_path = af2_file
    af2_path = os.path.join(af2_image_dir, af2_path)

    return af2_path


# This function gets a lot of default parameters which are seldomly changed, so
# we disable the PyLint check for having too many arguments.
def run_af2_singularity_image(  # pylint: disable=too-many-arguments
    fasta_files,
    output_dir,
    max_template_date=None,
    use_gpu=False,
    db_preset="full_dbs",
    model_preset="monomer",
    use_precomputed_msas=False,
    use_gpu_relax=False,
    num_multimer_predictions_per_model=5,
    af2_image_file=None,
    # af2_image_dir="/export/soft/singularity-containers/alphafold",
    af2_image_dir="/scicore/home/schwede/GROUP/alphafold_data/",
    snglrty_bin="/usr/bin/singularity",
    tmpdir_var="TMPDIR",
    extra_arg_list=None,
    data_dir="/scicore/data/managed/AF2params/frozen_230815T000000/",
    uniref90_database_path="/scicore/data/managed/UniProt/latest/uniref/"
    + "uniref90/uniref90.fasta",
    mgnify_database_path="/scicore/data/managed/MGnify/frozen_230815T000000/"
    + "mgnify/mgy_clusters_2022_05.fa",
    bfd_database_path="/scicore/data/managed/BFD/frozen_210805T085238/"
    + "bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt",
    small_bfd_database_path="/scicore/data/managed/BFD_small/"
    + "frozen_210812T135116/bfd-first_non_consensus_sequences.fasta",
    uniref30_database_path="/scicore/data/managed/AF_UniProt/"
    + "frozen_221115T101000/uniref/uniref30/UniRef30_2021_03",
    uniprot_database_path="/scicore/data/managed/UniProt/latest/knowledgebase/"
    + "complete/uniprot.fasta",
    pdb70_database_path="/scicore/data/managed/PDB70/frozen_210805T142857/"
    + "pdb70",
    pdb_seqres_database_path="/scicore/data/managed/PDB/latest/derived_data/"
    + "pdb_seqres.txt",
    template_mmcif_dir="/scicore/data/managed/PDB/latest/data/structures/all/"
    + "mmcif_files/",
    obsolete_pdbs_path="/scicore/data/managed/PDB/latest/data/status/"
    + "obsolete.dat",
):
    """Run the AlphaFold 2 Singularity image on host.

    The keyword arguments all come with defaults to run on the sciCORE cluster.

    If the Singularity container fails, a :class:`RuntimeError` will be
    raised.

    :param fasta_files: A list of target FASTA files. Every sequence is a
                        separate target. Only one sequence per file. Basenames
                        must be unique, AF2 uses them as names for the
                        result-directories.
    :type fasta_files: :class:`list` of :class:`str`
    :param output_dir: Path to the output directory which will hold all the
                       result directories for the targets.
    :type output_dir: :class:`str`
    :param max_template_date: max_template_date parameter of AF2. Templates
                              released by PDB after this date are not
                              considered.If None, set to tomorrow so all
                              templates are included.
    :type max_template_date: :class:`str`
    :param use_gpu: Run the AF2 pipeline using GPUs or not.
    :type use_gpu: :class:`bool`
    :param db_preset: Database configuration - smaller genetic databases
                      (reduced_dbs) and full genetic databases configuration
                      (full_dbs). Corresponds to the db_preset parameter in AF2.
    :type db_preset: :class:`str`
    :param model_preset: Model configuration - corresponds to the model_preset
                         parameter in AF2.
    :type model_preset: :class:`str`
    :param use_precomputed_msas: Use existing MSAs from cwd. Corresponds to the
                                 use_precomputed_msas parameter in AF2.
    :type use_precomputed_msas: :class:`bool`
    :param use_gpu_relax: Run relaxation on GPU. Corresponds to the
                          use_gpu_relax parameter in AF2.
    :type use_gpu_relax: :class:`bool`
    :param num_multimer_predictions_per_model: In multimer mode, no. of
                                               predictions per model.
                                               Corresponds to the
                                               num_multimer_predictions_per_model
                                               parameter in AF2.
    :type num_multimer_predictions_per_model: :class:`int`
    :param af2_image_file: Declare a Singularity image to run the AF2 pipeline
                           from. If None, an image from af2_image_dir will be
                           used.
    :type af2_image_file: :class:`str`
    :param af2_image_dir: Declare from which directory the Singularity image
                          should be used. Image file names need to be of form
                          alphafold-<YYYY-MM-DD>.sif. By default, the latest
                          image will be used. This parameter has no effect if
                          af2_image_file is not None.
    :type af2_image_dir: :class:`str`
    :param snglrty_bin: Path to the singularity binary.
    :type snglrty_bin: :class:`str`
    :param tmpdir_var: An environment variable defining tmp space, that is not
                       necessarily "/tmp" on some systems.
    :type tmpdir_var: :class:`str`
    :param extra_arg_list: List of extra arguments appended to the Singularity
                           call. That is for adding extra arguments in a
                           modified version of run_alphafold.py.
    :type extra_arg_list: :class:`list`
    :param data_dir: Holds the params sub directory with model parameters.
                     Corresponds to DOWNLOAD_DIR in AF2.
    :type data_dir: :class:`str`
    :param uniref90_database_path: Holds the UniRef90 database path.
                                   Corresponds to uniref90_database_path in AF2.
    :type uniref90_database_path: :class:`str`
    :param mgnify_database_path: Holds the MGnify database path. Corresponds to
                                 mgnify_database_path in AF2.
    :type mgnify_database_path: :class:`str`
    :param bfd_database_path: BFD database prefix. Corresponds to
                              bfd_database_path in AF2.
    :type bfd_database_path: :class:`str`
    :param small_bfd_database_path: Small BFD database path, used with
                                    db_preset="reduced_dbs", not a HHblits
                                    prefix as for the large BFD database.
                                    Corresponds to small_bfd_database_path in
                                    AF2.
    :type small_bfd_database_path: :class:`str`
    :param uniref30_database_path: UniRef30 database prefix. Corresponds to
                                     uniref30_database_path in AF2.
    :type uniref30_database_path: :class:`str`
    :param uniprot_database_path: UniProt sequence file. Corresponds to
                                  uniprot_database_path in AF2. Needed for
                                  model_preset == multimer.
    :type uniprot_database_path: :class:`str`
    :param pdb70_database_path: PDB70 database prefix. Corresponds to
                                pdb70_database_path in AF2. Will be disabled if
                                model_preset == multimer.
    :param pdb_seqres_database_path: PDB seqres file. Corresponds to
                                     pdb_seqres_database_path in AF2. Needed for
                                     model_preset == multimer.
    :type pdb_seqres_database_path: :class:`str`
    :type pdb70_database_path: :class:`str`
    :param template_mmcif_dir: Path to the template structures. Corresponds to
                               template_mmcif_dir in AF2.
    :type template_mmcif_dir: :class:`str`
    :param obsolete_pdbs_path: Mapping file of obsoleted PDB entries and
                               replacements. Corresponds to obsolete_pdbs_path
                               in AF2.
    :type obsolete_pdbs_path: :class:`str`

    :returns: Nothing.

    :raises: :class:`RuntimeError` if the Singularity/ AF2 call fails."""
    # Since the number of arguments to this function is already high, we allow
    # more local variables than defined by PEP8.
    # pylint: disable=too-many-locals
    if extra_arg_list is None:
        extra_arg_list = []
    tmpdir = os.getenv(tmpdir_var)
    if tmpdir is None:
        raise ValueError(f"tmpdir_var {tmpdir_var} must not be empty.")
    if db_preset not in ["full_dbs", "reduced_dbs"]:
        raise ValueError(
            f"db_preset {db_preset} not allowed, known values: "
            + "'full_dbs', 'reduced_dbs'"
        )
    if model_preset not in _shared.MODEL_PRESET_CHOICES:
        raise ValueError(
            f"model_preset {model_preset} not allowed, known "
            + f"values: {', '.join(_shared.MODEL_PRESET_CHOICES)}"
        )

    # gather data paths into dictionary for mountpoint collecting
    data_paths = {
        "data_dir": data_dir,
        "uniref90_database_path": uniref90_database_path,
        "mgnify_database_path": mgnify_database_path,
        "pdb70_database_path": pdb70_database_path,
        "template_mmcif_dir": template_mmcif_dir,
        "obsolete_pdbs_path": obsolete_pdbs_path,
    }
    if db_preset == "full_dbs":
        data_paths["bfd_database_path"] = bfd_database_path
        data_paths["uniref30_database_path"] = uniref30_database_path
    elif db_preset == "reduced_dbs":
        data_paths["small_bfd_database_path"] = small_bfd_database_path
    if model_preset == "multimer":
        del data_paths["pdb70_database_path"]
        data_paths["pdb_seqres_database_path"] = pdb_seqres_database_path
        data_paths["uniprot_database_path"] = uniprot_database_path

    # Collect bind mountpoints for singularity, mountpoints for input data plus
    # adjusting input file paths, deal with the output directory.
    binds, snglrty_fasta, bind_out, data_paths = _collect_bind_mountpints(
        tmpdir, fasta_files, output_dir, data_paths
    )

    if max_template_date is None:
        max_template_date = datetime.now() + timedelta(1)
        max_template_date = max_template_date.strftime("%Y-%m-%d")

    if af2_image_file is None:
        af2_image_file = _determine_af2_image(af2_image_dir)
    print("Using Singularity image:", af2_image_file, flush=True)

    # assemble the Singularity call
    snglrty_cmd = _assemble_singularity_call(
        snglrty_fasta,
        bind_out,
        max_template_date,
        af2_image_file,
        snglrty_bin,
        tmpdir,
        binds,
        use_gpu,
        db_preset,
        model_preset,
        use_precomputed_msas,
        use_gpu_relax,
        num_multimer_predictions_per_model,
        data_paths,
        extra_arg_list,
    )

    # set up environment
    os.environ["PYTHONUNBUFFERED"] = "1"
    os.environ["NVIDIA_VISIBLE_DEVICES"] = "all"
    os.environ["TF_FORCE_UNIFIED_MEMORY"] = "1"
    os.environ["XLA_PYTHON_CLIENT_MEM_FRACTION"] = "4.0"
    with subprocess.Popen(
        snglrty_cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        cwd=os.getcwd(),
    ) as proc:
        # Read output "live"/ in streaming fashion, wait after EOF for the
        # process to finish.
        for line in proc.stdout:
            print(line.decode().strip(), file=sys.stderr, flush=True)
        proc.wait()

        # check for fails
        if proc.returncode:
            raise RuntimeError(
                "Singularity/ AF2 did not finish successfully, "
                + f"exit code: {proc.returncode}",
                proc.returncode,
            )


def main():
    """Execute as script"""
    opts, extra_arg_list = _parse_cmdline()

    # Collect data paths into a dictionary so we can deal with default path on
    # the sciCORE cluster more easily.
    data_paths = {}
    if opts.data_dir is not None:
        data_paths["data_dir"] = opts.data_dir
    if opts.uniref90_database_path is not None:
        data_paths["uniref90_database_path"] = opts.uniref90_database_path
    if opts.mgnify_database_path is not None:
        data_paths["mgnify_database_path"] = opts.mgnify_database_path
    if opts.bfd_database_path is not None:
        data_paths["bfd_database_path"] = opts.bfd_database_path
    if opts.small_bfd_database_path is not None:
        data_paths["small_bfd_database_path"] = opts.bfd_database_path
    if opts.uniref30_database_path is not None:
        data_paths["uniref30_database_path"] = opts.uniref30_database_path
    if opts.uniprot_database_path is not None:
        data_paths["uniprot_database_path"] = opts.uniprot_database_path
    if opts.pdb70_database_path is not None:
        data_paths["pdb70_database_path"] = opts.pdb70_database_path
    if opts.pdb_seqres_database_path is not None:
        data_paths["pdb_seqres_database_path"] = opts.pdb_seqres_database_path
    if opts.template_mmcif_dir is not None:
        data_paths["template_mmcif_dir"] = opts.template_mmcif_dir
    if opts.obsolete_pdbs_path is not None:
        data_paths["obsolete_pdbs_path"] = opts.obsolete_pdbs_path

    try:
        run_af2_singularity_image(
            opts.fasta_files,
            opts.output_dir,
            opts.max_template_date,
            opts.use_gpu,
            opts.db_preset,
            opts.model_preset,
            opts.use_precomputed_msas,
            opts.use_gpu_relax,
            opts.num_multimer_predictions_per_model,
            opts.singularity_image,
            extra_arg_list=extra_arg_list,
            **data_paths,
        )
    except RuntimeError as rte:
        sys.stderr.write(rte.args[0] + "\n")
        sys.exit(rte.args[1])


#  LocalWords:  AlphaFold Slurm sciCORE param FASTA Basenames RuntimeException
#  LocalWords:  str dir snglrty tmpdir tmp argparse os ArgumentParser metavar
#  LocalWords:  RawDescriptionHelpFormatter nargs getenv startswith ValueError
#  LocalWords:  RuntimeError bfd BFD dbs mgnify HHblits uniref Uniref pdb
#  LocalWords:  multimer seqres uniprot msas MSAs precomputed
