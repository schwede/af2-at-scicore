"""This is a module of shared functions of the other modules from this package.

This module should not be used outside of the package itself. If you need some
functionality to be external, we should create a proper module for that.
"""

# Copyright (c) 2021, SIB - Swiss Institute of Bioinformatics and
#                     Biozentrum - University of Basel
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import sys

MODEL_PRESET_CHOICES = ["monomer", "monomer_casp14", "monomer_ptm", "multimer"]


def get_version():
    """Load the package version (import only upon request).

    Only delivers a versions string after packaging happened once (run
    `python -m build` in the same directory as the setup.cfg file), since that
    creates the version file.

    :returns: A version string or 'x.x.x' in case packaging did not happen."""
    # version is only needed to call the script with --version which is
    # terminal. So we allow this wrong way to import it here.
    # pylint: disable=import-outside-toplevel
    try:
        from . import version
    except ImportError:
        return "x.x.x"
    return version.version


def parse_af2_arguments(parser):
    """AF2 pipeline arguments parsing capabilities.

    Add arguments common for running the AF2 pipeline to an existing
    :class:`argparse.ArgumentParser` object. Run the parser and return the
    result. That way tools can implement their own description for the parser
    and run tests on input with the :class:`argparse.Namespace` object.

    :returns: :class:`argparse.Namespace` object from
              `argparse.ArgumentParser.parse_args`.
    """
    db_preset_choices = {
        "reduced_dbs": ["bfd_database_path", "uniref30_database_path"],
        "full_dbs": ["small_bfd_database_path"],
    }

    af2_group = parser.add_argument_group("AlphaFold settings")
    af2_group.add_argument(
        "output_dir",
        type=str,
        help="Directory to store results in, gets one subdirectory per target.",
        metavar="<OUTPUT DIR>",
    )
    af2_group.add_argument(
        "fasta_files",
        nargs="+",
        help="Target FASTA files, one sequence per file, unique basenames.",
        metavar="<FASTA FILE>",
    )
    af2_group.add_argument(
        "-d",
        "--max-template-date",
        type=str,
        help="Latest PDB release date to consider templates from. If omitted, "
        + "all templates will be considered.",
        metavar="<YYYY-MM-DD>",
        default=None,
    )
    af2_group.add_argument(
        "-g",
        "--use-gpu",
        action="store_true",
        help="Run using GPUs",
        default=False,
    )
    af2_group.add_argument(
        "-p",
        "--db-preset",
        choices=db_preset_choices.keys(),
        help="Choose databases - smaller genetic databases (reduced_dbs) or "
        + "full genetic databases (full_dbs).",
        default="full_dbs",
    )
    af2_group.add_argument(
        "-m",
        "--model-preset",
        choices=MODEL_PRESET_CHOICES,
        help="Choose the model to be run, follows the --model_preset of the "
        + "AF2 pipeline.",
        default="monomer",
    )
    af2_group.add_argument(
        "--use-precomputed-msas",
        action="store_true",
        help="Use existing MSAs from current working directory.",
        default=False,
    )
    af2_group.add_argument(
        "--use-gpu-relax",
        action="store_true",
        help="Use GPU for relaxation (if GPU is enabled).",
        default=False,
    )
    af2_group.add_argument(
        "--num-multimer-predictions-per-model",
        type=int,
        help="Number of models per prediction. Only applies with "
        + "--model-preset=multimer.",
        metavar="<NUMBER>",
        default=5,
    )
    af2_group.add_argument(
        "--version",
        action="version",
        version=f"{get_version()}",
        help="Show the version number and exit.",
    )
    af2_group.add_argument(
        "--data-dir",
        type=str,
        help="Path containing the 'params' subdirectory with the model "
        + "parameters.",
        metavar="<PATH>",
        default=None,
    )
    af2_group.add_argument(
        "--uniref90-database-path",
        type=str,
        help="Path to the UniRef90 database.",
        metavar="<PATH>",
        default=None,
    )
    af2_group.add_argument(
        "--mgnify-database-path",
        type=str,
        help="Path to the MGnify database.",
        metavar="<PATH>",
        default=None,
    )
    af2_group.add_argument(
        "--bfd-database-path",
        type=str,
        help="Path to the BFD database, prefix for the HHblits search.",
        metavar="<PATH/ PREFIX>",
        default=None,
    )
    af2_group.add_argument(
        "--small-bfd-database-path",
        type=str,
        help="Path to the small version of the BFD database, used with "
        + "'--db-preset reduced_dbs'.",
        metavar="<PATH/ PREFIX>",
        default=None,
    )
    af2_group.add_argument(
        "--uniref30-database-path",
        type=str,
        help="Path to the UniRef30 database, prefix for the HHblits search.",
        metavar="<PATH/ PREFIX>",
        default=None,
    )
    af2_group.add_argument(
        "--uniprot-database-path",
        type=str,
        help="Path to the UniProt database, for the JackHMMer call.",
        metavar="<PATH>",
        default=None,
    )
    af2_group.add_argument(
        "--pdb70-database-path",
        type=str,
        help="Path to the PDB70 database, prefix for the HHsearch call.",
        metavar="<PATH/ PREFIX>",
        default=None,
    )
    af2_group.add_argument(
        "--pdb-seqres-database-path",
        type=str,
        help="Path to the PDB seqres database, for the hhmsearch call.",
        metavar="<PATH>",
        default=None,
    )
    af2_group.add_argument(
        "--template-mmcif-dir",
        type=str,
        help="Path to template structures in mmCIF format.",
        metavar="<PATH>",
        default=None,
    )
    af2_group.add_argument(
        "--obsolete-pdbs-path",
        type=str,
        help="File mapping obsoleted PDB entries to their replacements.",
        metavar="<File>",
        default=None,
    )
    af2_group.add_argument(
        "--singularity-image",
        type=str,
        help="Specify the Singularity image. By default the latest is used.",
        metavar="<FILE>",
        default=None,
    )
    opts, extra_opts = parser.parse_known_args()

    # check that the db_preset setting makes sense
    for arg in db_preset_choices[opts.db_preset]:
        if getattr(opts, arg) is not None:
            print(
                f"WARNING: '--db-preset {opts.db_preset}' disables use of "
                + f"'--{(arg).replace('_', '-')}'.",
                file=sys.stderr,
                flush=True,
            )

    # check that model_preset settings go along with databases
    if opts.model_preset == "multimer":
        if opts.pdb70_database_path is not None:
            print(
                f"WARNING: '--model-preset {opts.model_preset}' disables use "
                + "of '--pdb70_database_path'.",
                file=sys.stderr,
                flush=True,
            )

    # check that --use-gpu-relax is only enabled together with --use-gpu
    if opts.use_gpu_relax and not opts.use_gpu:
        print(
            "WARNING: Enabling --use-gpu-relax on non-gpu compute nodes "
            + "will fail. You are seeing this warning since you enabled "
            + "--use-gpu-relax but not --use-gpu.",
            file=sys.stderr,
            flush=True,
        )

    if opts.singularity_image is not None:
        if not os.path.exists(opts.singularity_image):
            print(
                f"Singularity image '{opts.singularity_image}' does not "
                + "exists. Aborting.",
                file=sys.stderr,
            )
            sys.exit(1)

    return opts, extra_opts


#  LocalWords:  argparse ArgumentParser Namespace Bioinformatics Biozentrum af
#  LocalWords:  sublicense NONINFRINGEMENT sys cfg pylint toplevel ImportError
#  LocalWords:  dbs bfd uniref AlphaFold dir str metavar fasta nargs YYYY DL
#  LocalWords:  basenames gpu ensembling casp sciCORE prog params uniref pdb
#  LocalWords:  mgnify MGnify HHblits HHsearch mmcif pdbs args arg getattr
#  LocalWords:  stderr
