# AF2 at sciCORE - brought to you by the SWISS-MODEL team

[[_TOC_]]

This is a specimen of the AlphaFold 2 [
[Pub](https://www.nature.com/articles/s41586-021-03819-2),
[Git](https://github.com/deepmind/alphafold/)] pipeline (AF2), enabled to
execute on the sciCORE cluster.

The adaptation is necessary since AF2 uses [Docker](https://www.docker.com)
where the sciCORE cluster offers [Singularity](https://singularity.hpcng.org)
containers - background is just funny permissions in Docker... The difference is
not dramatic, there is no performance penalty, but the original wrapper scripts
of AF2 for Docker don't play along with Singularity.

In this repository, you'll find a
[Python package](https://git.scicore.unibas.ch/schwede/af2-at-scicore/-/packages)
to run the default AF2 pipeline, the one from the
[publication]((https://www.nature.com/articles/s41586-021-03819-2)), and an
[IPython notebook](notebook_playground.ipynb) that allows to play around with
the pipeline.

AF2 needs quite some databases. From the original
[GitHub repository](https://github.com/deepmind/alphafold#genetic-databases)
you'll learn that there are three different sets. For now, we only provide the
default databases (aka the big ones).

There is one thing to keep in mind when running AF2. There is the option to run
using GPUs. That will be faster than just utilising CPUs. But the sciCORE
cluster only has around 10 compute slots that have the right GPU for the AF2
pipeline. So usually running without CPUs gets the job done faster because of
less waiting time.


## Default AF2 pipeline

For running the AF2 pipeline, we provide a Python package, called `run_af2`.
That also installs binaries that allows to run the pipeline in a similar way as
the original software. The Python package itself allows programmatic access of
the AF2 pipeline.

### Install the Python package

A ready-2b-installed package called `run_af2` is available in the
[sciCORE GitLab repository](
https://git.scicore.unibas.ch/schwede/af2-at-scicore/-/packages) of this project.
There you'll find a list of all available versions and clicking a package leads
to a more detailed page including download links. However, you can install
directly from the command line using `pip`, no need to download the package first
(including creating a [Python Virtual Environment](
https://docs.python.org/3.7/tutorial/venv.html)):

```terminal
$ python3 -m venv af2_venv
$ source af2_venv/bin/activate
(af2_venv) $ pip install run-af2 --extra-index-url https://git.scicore.unibas.ch/api/v4/projects/1783/packages/pypi/simple
Looking in indexes: https://pypi.org/simple, https://git.scicore.unibas.ch/api/v4/projects/1783/packages/pypi/simple
Collecting run-af2
  Using cached https://git.scicore.unibas.ch/api/v4/projects/1783/packages/pypi/files/9fee83d286b21709792aeb28a054eded97bec7c64e4cc51334de355d55df1f2e/run_af2-0.1.5-py3-none-any.whl (9.5 kB)
Installing collected packages: run-af2
Successfully installed run-af2-0.1.5
(af2_venv) $
```

And that's how you install the latest version of `run_af2`. After that, commands
`run-af2` and `submit-af2` should be available with activated Virtual
Environment.

<!--  build:
      - go to `master`
      - create tag
      - run:
rm -rf dist/* && python -m build && pip install --force-reinstall dist/run_af2-*-py3-none-any.whl
     - build documentation:
rm -rf doc/html/*
sphinx-build doc/sphinx/ doc/html/
     - commit doc/html
     - remove & re-add tag
     - rebuild to see version is right
     - push tag
     - merge master into develop
 -->

### Run AlphaFold 2

The `run_af2` Python package installs a command `run-af2` that operates the AF2
pipeline by running the Singularity container. Predicting a protein model is as
easy as calling

```terminal
$ run-af2 <OUTPUT DIRECTORY> <FASTA FILE> [<FASTA FILE> ...]
```

The options of the command are available via `--help`:

```terminal
$ run-af2 --help
```

The arguments mostly correspond to [`run_alphafold.py`](
https://github.com/deepmind/alphafold/blob/main/run_alphafold.py) and
[`run_docker.py`](
https://github.com/deepmind/alphafold/blob/main/docker/run_docker.py) of the
original [GitHub repository](https://github.com/deepmind/alphafold). All the
data/ paths related arguments have defaults that run on the sciCORE cluster.

### Submit an AlphaFold 2 job to the sciCORE cluster

The `run_af2` Python package installs a command `submit-af2` that submits
`run-af2` jobs to the cluster via `sbatch`. Sending a job to the cluster as
simple as calling

```terminal
$ submit-af2 <OUTPUT DIRECTORY> <FASTA FILE>
```

`submit-af2` executed without any argument shows the usage string/ options.

If you want to submit jobs on your own (as array job or with different names,
output files ...), here are some things to consider:

- the AF2 documentation suggests 12 CPUs and 85GB of RAM for running with the
  default databases
- the GPU partitions that work with AF2 are `a100` and `rtx8000`
  (`--partition=a100,rtx8000`)


## Custom AF2 pipeline

`run_af2` executes the default AlphaFold 2 pipeline. That is, in the end a
script [`run_alphafold.py`](
https://github.com/deepmind/alphafold/blob/main/run_alphafold.py) is run. For
some studies, you may want to go by a modified version of `run_alphafold.py`.
While `run_af2` is based on containerised software in its backyard, injecting
your custom script inside the Singularity container is not terribly complicated.
In the shell, you want to run your command in, simply create an environment
variable `SINGULARITY_BINDPATH` with the mapping of your script to the original
script inside the container:

```terminal
$ export SINGULARITY_BINDPATH="/PATH/TO/SCRIPT/run_alphafold.py:/app/alphafold/run_alphafold.py""
```

Where `/PATH/TO/SCRIPT/run_alphafold.py` points to your modified
`run_alphafold.py`. After that, just run `run_af2` as described in
[Default AF2 pipeline](#default-af2-pipeline).

Please note, this method of altering the app executed by a container defies the
main purpose of containers providing unaltered apps ready to go, out of the box.
Also note, since this method relies on internals of the pipeline beyond the
scope of af2@scicore, future updates of AF2 may break it.


## Custom container

`run_af2` uses the AlphaFold 2 pipeline as provided by DeepMind. That is, we
build the vanilla Docker container as described in their [documentation](
https://github.com/deepmind/alphafold). Simply run

```terminal
$ docker build -f docker/Dockerfile -t alphafold:2022-01-11 .
```

inside a checkout of the AF2 Git repository. That creates the container tagged
with `2022-01-11`, which is the way `run_af2` indicates versions. It's simply
the date of creation of the container from a fresh checkout of the Git repository.

This Docker container is then turned into a Singularity container:

```terminal
$ singularity build alphafold-2022-01-11.sif docker-daemon://alphafold:2022-01-11
```

`alphafold-2022-01-11.sif` can then be used with the `--singularity-image` of
`run_af2`.


## Interactive IPython notebook

As an expert mode, we provide a notebook version of the default AF2 pipeline 
to interactively explore possible modifications. The notebook server can
be started from the Singularity image. The following step by step guide
assumes a working SSH key setup to connect to sciCORE login nodes with
SSH Agent Forwarding enabled.

### Allocate resources on a compute node with GPU 

A GPU is actually not a hard requirement, the pipeline runs just slower
without...
If you don't want to use GPUs, drop the --gres and --partition flags.


```terminal
$ salloc --gres=gpu:1 --partition=a100 --cpus-per-task=12 --mem=85G --qos=1day
salloc: Pending job allocation 55084797
salloc: job 55084797 queued and waiting for resources
salloc: job 55084797 has been allocated resources
salloc: Granted job allocation 55084797
salloc: Waiting for resource configuration
salloc: Nodes shi132 are ready for job
```

### Identify compute node from ouput

Read it from the salloc output => its shi132 (that's actually a non-GPU node 
but hey)


### Connect to compute node

```terminal
$ ssh shi132
```

### Get IP form compute node

```terminal
$ hostname -i
```

### Start IPython notebook server using our Singularity image

The notebook hardcodes paths to data downloaded for AF2. At the time of writing, 
this data was here:
/scicore/home/schwede/GROUP/alphafold_data
The corresponding path in the notebook is expected to be:
/mnt/scicore/home/schwede/GROUP/alphafold_data 
therefore the mounting with --bind. 
<IP_OF_COMPUTE_NODE> is what we just figured out above and <PORT> can be chosen 
by you.

```terminal
$ singularity exec --nv --bind $TMPDIR:/tmp --bind $TMPDIR:/scratch --bind /scicore:/mnt/scicore <SINGULARITY_IMAGE> ipython notebook --no-browser --port <PORT> --ip=<IP_OF_COMPUTE_NODE> --NotebookApp.token="my_password"
```

### Establish a SSH tunnel from your local machine to the compute node

We're using worker-schwede.scicore.unibas.ch as jump host, as we cannot
directly connect to the compute node. Adapt to any login node if needed 
(or if you're not in the Schwede group).

```terminal
ssh -N -L <PORT>:<IP_OF_COMPUTE_NODE>:<PORT> <USERNAME>@worker-schwede.scicore.unibas.ch
```

### Connect to server from your browser

You have now two shells open. One with the running notebook, another one with 
the tunneling. In the shell with the running notebook you have a link that 
looks like http://127.0.0.1:8842/?token=...

OPEN IT!

You'll be asked to enter a password. If you didn't change anything, its the 
highly secure *my_password*.


## Tips & tricks

* Database versions: the AlphaFold2 release notes keep track of database
  updates, so if in need for version information, rummage through
  https://github.com/deepmind/alphafold/releases/tag/<TAG ID>


<!--  LocalWords:  AlphaFold sciCORE IPython ipynb GitLab PyPI af FASTA py rtx
 -->
<!--  LocalWords:  alphafold sbatch DeepMind
 -->
