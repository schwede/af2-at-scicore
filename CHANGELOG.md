Changes in Release 4.1.0
=================================================================================

* `run_af2` Python package:
   * Adapted to AlphaFold2 2.3.2


Changes in Release 4.0.2
=================================================================================

* `run_af2` Python package:
  * Pre-defined data paths now point to sciCORE's managed data


Changes in Release 4.0.0
=================================================================================

* `run_af2` Python package:
  * Rename command line option `--uniclust30-database-path` to
    `--uniref30-database-path` according to the change in AF2 2.3.0
  * Adapt `submit-af2` to newer SLURM version


Changes in Release 3.1.1
=================================================================================

* `run_af2` Python package:
  * Adapt to new AF2 versioning scheme
  * Fallback for picking up the Singularity image automatically


Changes in Release 3.1.0
=================================================================================

* `run_af2` Python package:
  * Add command line option `--num-multimer-predictions-per-model`
  * Switch default value of option `--use-gpu-relax` to False


Changes in Release 3.0.0
=================================================================================

* `run_af2` Python package:
  * Prepare to run AF2 pipeline 2.2.0
  * Remove option `--is-prokaryote-list`


Changes in Release 2.2.0
=================================================================================

* `run_af2` Python package:
  * Add command line options `--use-precomputed-msas`, `--no-run-relax` and
    `--use-gpu-relax`


Changes in Release 2.1.0
=================================================================================

* `run_af2` Python package:
  * Allow extra arguments for `run-af2` for customised AF2 pipelines


Changes in Release 2.0.0
=================================================================================

* `run_af2` Python package:
  * Prepare to run AF2 pipeline 2.1
  * Rename command line options `--pdb70_database_path` and
    `--obsolete_pdbs_path` to `--pdb70-database-path` and `--obsolete-pdbs-path`


Changes in Release 1.0.5
=================================================================================

* `run_af2` Python package:
   * Add option to specify the Singularity image rather than auto-detect the
     latest


Changes in Release 1.0.0
=================================================================================

* `run_af2` Python package:
   * Changed the order of arguments for `submit-af2`
   * Allowed multiple FASTA files as input to `submit-af2`
   * Enabled option `--max-template-date` for `submit-af2`
   * Enabled `preset` functionality of AF2 in commands and Python module
   * Fixed working-directory issue with the container


Changes in Release 0.1.5
=================================================================================

* Added license (MIT)
* `run_af2` Python package:
   * Made data paths arguments/ parameters
   * Renamed run_singularity command to submit-af2
